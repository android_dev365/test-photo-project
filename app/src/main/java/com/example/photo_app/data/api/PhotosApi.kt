package com.example.photo_app.data.api
import com.example.photo_app.data.response.PhotosResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface PhotosApi {

    @GET(SEARCH_URL)
     suspend fun searchPhotosList(
        @Query("text") text: String,
        @Query("page") page: Int,
        @Query("api_key") apiKey: String = API_KEY,
        @Query("format") format: String = PhotosApi.format,
        @Query("nojsoncallback") nojsoncallback: Int = 1

    ): Response<PhotosResponse>

     companion object{
         const val SEARCH_URL = "?method=flickr.photos.search"
         const val format = "json"
         const val API_KEY = "cee833b176b2bbbfc56c7876b5c88ed6"
     }
}