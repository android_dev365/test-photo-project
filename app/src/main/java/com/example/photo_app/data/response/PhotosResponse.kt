package com.example.photo_app.data.response

import com.google.gson.annotations.SerializedName


data class PhotosResponse(

    @SerializedName("photos")
    val photos: PhotosData,
    @SerializedName("stat")
    val stat: String
){

    data class PhotosData(
        val page:Int,
        @SerializedName("pages")
        val pages: String,
        @SerializedName("perpage")
        val perpage:Int,
        @SerializedName("total")
        val total: String,
        @SerializedName("photo")
        val data: List<PhotoItem>,
    )

    data class PhotoItem(
        val id: String,
        val owner: String,
        val secret: String,
        val server: String,
        val popularity: Double,
        val farm: Int,
        val title: String,
        @SerializedName("ispublic")
        val isPublic: String,
        @SerializedName("isfriend")
        val isFriend: Int,
        @SerializedName("isfamily")
        val isFamily: Int
    )
}