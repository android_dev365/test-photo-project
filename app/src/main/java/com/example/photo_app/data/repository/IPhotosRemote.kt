package com.example.photo_app.data.repository

import androidx.paging.PagingData
import com.example.photo_app.domain.data.entity.Photo
import kotlinx.coroutines.flow.Flow

interface IPhotosRemote {

    fun searchPhotoList(query:String): Flow<PagingData<Photo>>
}