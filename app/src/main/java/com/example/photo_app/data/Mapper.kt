package com.example.photo_app.data

import com.example.photo_app.data.response.PhotosResponse
import com.example.photo_app.domain.data.entity.Photo


fun PhotosResponse.PhotoItem.toEntity(): Photo {
    return Photo(
        id = id,
        owner = owner,
        secret = secret,
        server = server,
        title = title
    )
}




