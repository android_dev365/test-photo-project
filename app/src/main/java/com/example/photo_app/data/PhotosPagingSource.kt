package com.example.photo_app.data


import androidx.paging.PagingSource
import com.example.photo_app.data.api.PhotosApi
import com.example.photo_app.domain.data.entity.Photo




class PhotosPagingSource(
    private val service: PhotosApi,
    private val query:String
) : PagingSource<Int, Photo>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Photo> {
        val page = params.key ?: STARTING_PAGE_INDEX
        return try {
            val response = service.searchPhotosList(query,page)
            if(response.isSuccessful) {
                val pages = response.body()?.photos?.pages?.toInt()
                val photos = response.body()?.photos?.data?.map { it.toEntity() }

                LoadResult.Page(
                    data = photos ?: listOf(),
                    prevKey = if (page == STARTING_PAGE_INDEX) null else page - 1,
                    nextKey = if (page == pages) null else page + 1
                )
            }
            else{
                throw Exception()
            }

        } catch (exception: Exception) {
            LoadResult.Error(exception)
        }
    }

    companion object{
        private const val STARTING_PAGE_INDEX = 1
    }

}
