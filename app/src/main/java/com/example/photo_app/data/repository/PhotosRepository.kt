package com.example.photo_app.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData

import com.example.photo_app.data.PhotosPagingSource
import com.example.photo_app.data.api.PhotosApi
import com.example.photo_app.domain.data.entity.Photo

import kotlinx.coroutines.flow.Flow

class PhotosRepository(
    private val api:PhotosApi
) : IPhotosRemote {

    override fun searchPhotoList(query:String): Flow<PagingData<Photo>> {
        return Pager(
            config = PagingConfig(enablePlaceholders = false, pageSize = NETWORK_PAGE_SIZE),
            pagingSourceFactory = { PhotosPagingSource(api,query) }
        ).flow
    }

    companion object {
        private const val NETWORK_PAGE_SIZE = 20
    }
}