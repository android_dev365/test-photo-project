package com.example.photo_app.utils

import com.example.photo_app.domain.data.entity.Photo

object ApiHelper {
    private const val BASE_PHOTO_PATH = "https://live.staticflickr.com/"
    private const val size = "w"

    fun getPhotoPath(photo:Photo): String {
        return BASE_PHOTO_PATH + photo.server+"/"+photo.id+"_"+photo.secret+"_"+ size+".jpg"

    }
}