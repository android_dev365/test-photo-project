package com.example.photo_app.di

import android.app.Application
import org.kodein.di.Kodein
import org.kodein.di.android.androidCoreModule

object AppModule {

    fun get(application: Application) = Kodein.Module("App") {
        import(androidCoreModule(application))
        import(RemoteModule.get())
    }
}