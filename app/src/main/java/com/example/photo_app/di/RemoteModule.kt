package com.example.photo_app.di

import android.content.Context
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.example.photo_app.data.api.PhotosApi

import com.example.photo_app.data.repository.IPhotosRemote
import com.example.photo_app.data.repository.PhotosRepository
import com.example.photo_app.domain.usecase.SearchUseCase
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RemoteModule {

    fun get() = Kodein.Module("Remote") {

        bind<Retrofit>() with singleton {
            Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .client(instance())
                .build()
        }

        bind<OkHttpClient>() with singleton {
            val builder = OkHttpClient.Builder()
            builder.cache(instance())
            builder.connectTimeout(100, TimeUnit.SECONDS)
            builder.retryOnConnectionFailure(true)

            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(loggingInterceptor)
            builder.build()
        }

        bind() from provider {
            val cacheSize = 10 * 1024 * 1024 // 10 MB
            Cache(instance<Context>().cacheDir, cacheSize.toLong())
        }

        bind<PhotosApi>() with singleton { instance<Retrofit>().create(PhotosApi::class.java) }
        bind<IPhotosRemote>() with provider { PhotosRepository(instance()) }
        bind<SearchUseCase>() with singleton { SearchUseCase(instance()) }

    }

    private const val BASE_URL = "https://api.flickr.com/services/rest/"
}