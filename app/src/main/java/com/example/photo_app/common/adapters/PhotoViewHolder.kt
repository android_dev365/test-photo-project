package com.example.photo_app.common.adapters

import androidx.recyclerview.widget.RecyclerView
import com.example.photo_app.databinding.ListItemPhotoBinding
import com.example.photo_app.domain.data.entity.Photo

class PhotoViewHolder(
    private val binding: ListItemPhotoBinding
) : RecyclerView.ViewHolder(binding.root)
 {
     fun bind(item: Photo) {
         binding.apply {
             photo = item
             executePendingBindings()
         }
     }
}