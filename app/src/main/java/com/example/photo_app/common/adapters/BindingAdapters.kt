package com.example.photo_app.common.adapters

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.example.photo_app.R
import com.example.photo_app.domain.data.entity.Photo
import com.example.photo_app.utils.ApiHelper

@BindingAdapter("imageFromUrl")
fun bindImageFromUrl(view: ImageView, photo: Photo) {
    if (photo!= null) {
        val posterUrl = ApiHelper.getPhotoPath(photo)
        Glide.with(view.context)
            .load(posterUrl)
            .apply(
                RequestOptions()
                    .error(R.drawable.ic_launcher_background)
            )
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(view)
    }
}