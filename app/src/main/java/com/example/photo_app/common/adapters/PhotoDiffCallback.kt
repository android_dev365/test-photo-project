package com.example.photo_app.common.adapters
import androidx.recyclerview.widget.DiffUtil
import com.example.photo_app.domain.data.entity.Photo

class PhotoDiffCallback: DiffUtil.ItemCallback<Photo>() {

    override fun areItemsTheSame(oldItem: Photo, newItem: Photo): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem:Photo, newItem: Photo): Boolean {
        return oldItem == newItem
    }
}