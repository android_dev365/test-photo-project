package com.example.photo_app.domain.usecase

import androidx.paging.PagingData
import com.example.photo_app.data.repository.IPhotosRemote
import com.example.photo_app.domain.data.entity.Photo
import kotlinx.coroutines.flow.Flow


class SearchUseCase(
        private val repo: IPhotosRemote
): BaseUseCase<Flow<PagingData<Photo>>, SearchUseCase.Params>() {

    override suspend fun buildUseCase(params: Params): Flow<PagingData<Photo>> {
        return repo.searchPhotoList(params.query)
    }

    class Params private constructor(val query: String) {
        companion object {
            fun getSearchResult(query:String) = Params(query)
        }
    }
}