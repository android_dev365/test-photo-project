package com.example.photo_app.domain.usecase


import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

abstract class BaseUseCase<T, Params> {

    internal abstract suspend fun buildUseCase(params: Params): T

    fun execute(
            params: Params,
            scope: CoroutineScope,
            onSuccess: (T) -> Unit,
            onError: (e: Exception) -> Unit,
            executeContext: CoroutineContext = Dispatchers.IO,
            resultContext: CoroutineContext = Dispatchers.Main
    ) {
        scope.launch(executeContext) {
            try {
                val result = buildUseCase(params)
                scope.launch(resultContext) {
                    onSuccess(result)
                }
            } catch (e: Exception) {
                scope.launch(resultContext) {
                    onError(e)
                }
            }
        }
    }
}
