package com.example.photo_app.domain.data.entity

data class Photo(
    val id: String,
    val owner: String,
    val secret: String,
    val server: String,
    val title: String,

)
