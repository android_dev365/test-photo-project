package com.example.photo_app.presentation

import androidx.lifecycle.*
import androidx.paging.PagingData
import com.example.photo_app.domain.data.entity.Photo
import com.example.photo_app.domain.usecase.SearchUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow

class PhotosViewModel(
        private val searchUseCase: SearchUseCase
) : ViewModel() {

    private val _searchQuery = MutableStateFlow("")

    private val _searchResult: MutableStateFlow<Flow<PagingData<Photo>>?> = MutableStateFlow(null)

    val searchResult: Flow<Flow<PagingData<Photo>>?>
        get() = _searchResult

    fun onQueryTextChange(query: String) {
        _searchQuery.value = query
        search(query)
    }

    private fun search(query: String) {
        val params = SearchUseCase.Params.getSearchResult(query)
        searchUseCase.execute(
                params,
                viewModelScope,
                onSuccess = { flow ->
                    _searchResult.value = flow
                },
                onError = {
                }
        )
    }
}
