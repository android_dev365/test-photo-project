package com.example.photo_app.presentation

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil.setContentView
import androidx.lifecycle.*
import com.example.photo_app.R
import com.example.photo_app.common.adapters.PhotosLoadingAdapter
import com.example.photo_app.common.adapters.PhotosPageAdapter
import com.example.photo_app.databinding.ActivityMainBinding
import com.example.photo_app.common.KodeinActivity
import com.example.photo_app.common.bindViewModel
import com.example.photo_app.common.viewModel
import kotlinx.coroutines.flow.collectLatest
import org.kodein.di.Kodein
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider

class MainActivity : KodeinActivity() {

    override val kodeinModule = Kodein.Module("Main") {
        bindViewModel<PhotosViewModel>() with provider {
            PhotosViewModel(
                searchUseCase = instance()
            )
        }
    }

    private val viewModel: PhotosViewModel by viewModel()
    private val adapter = PhotosPageAdapter()
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setContentView(this, R.layout.activity_main_)
        setupViews()

        lifecycleScope.launchWhenCreated {

            viewModel.searchResult?.collectLatest { list ->
               list?.collectLatest { data -> adapter.submitData(data) }
            }
        }
    }

    private fun setupViews() {

        binding.photoList.adapter = adapter
        binding.photoList.adapter = adapter.withLoadStateHeaderAndFooter(
            header = PhotosLoadingAdapter { adapter.retry() },
            footer = PhotosLoadingAdapter { adapter.retry() }
        )

        binding.searchET.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                viewModel.onQueryTextChange(p0.toString())
            }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })

        binding.clearSearch.setOnClickListener {
            binding.searchET.text?.clear()
            binding.clearSearch.isVisible = false
        }
    }
}